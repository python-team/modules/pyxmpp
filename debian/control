Source: pyxmpp
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Bernd Zeimetz <bzed@debian.org>
Build-Depends: debhelper (>= 7.0.50~), python-dnspython (>=1.0), python-m2crypto, python-libxml2 (>= 2.6.11~),
 libxml2-dev, python-all-dev (>= 2.6.6-3~)
Standards-Version: 3.9.3
Homepage: http://pyxmpp.jajcus.net/
Vcs-Git: https://salsa.debian.org/python-team/packages/pyxmpp.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/pyxmpp

Package: python-pyxmpp
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, ${python:Depends},
 python-dnspython (>= 1.0), python-m2crypto, python-libxml2 (>= 2.6.11~)
Provides: ${python:Provides}
Recommends: python-zope.interface
Suggests: python-pyxmpp-doc
Description: XMPP and Jabber implementation for Python
 PyXMPP is a Python XMPP (RFC 3920,3921) and Jabber
 (http://www.jabber.org/protocol/) implementation. It is based on libxml2 --
 fast and fully-featured XML parser.
 .
 PyXMPP provides most core features of the XMPP protocol and several
 JSF-defined extensions. PyXMPP provides building blocks for creating Jabber
 clients and components. Developer uses them to setup XMPP streams, handle
 incoming events and create outgoing stanzas (XMPP "packets").
 .
 Features:
  * nearly complete XMPP Core (RFC 3920) protocol for client connections
    (includes SASL, TLS and Stringprep)
  * mostly complete XMPP IM (RFC 3921) protocol (lacks privacy lists)
  * XMPP error objects including translations to and from legacy codes for
    backward compatibility (JEP-0086).
  * legacy authentication ("digest" and "plain") (JEP-0078).
  * component protocol (JEP-0114).
  * Service Discovery (JEP-0030).
  * vCards -- both Jabber "vcard-temp" and RFC 2426
  * basic parts of the Multi-User Chat protocol (JEP-0045)
  * delayed delivery timestamps (JEP-0091).

Package: python-pyxmpp-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Description: XMPP and Jabber implementation for Python (documentation)
 PyXMPP is a Python XMPP (RFC 3920,3921) and Jabber
 (http://www.jabber.org/protocol/) implementation. It is based on libxml2 --
 fast and fully-featured XML parser.
 .
 PyXMPP provides most core features of the XMPP protocol and several
 JSF-defined extensions. PyXMPP provides building blocks for creating Jabber
 clients and components. Developer uses them to setup XMPP streams, handle
 incoming events and create outgoing stanzas (XMPP "packets").
 .
 Features:
  * nearly complete XMPP Core (RFC 3920) protocol for client connections
    (includes SASL, TLS and Stringprep)
  * mostly complete XMPP IM (RFC 3921) protocol (lacks privacy lists)
  * XMPP error objects including translations to and from legacy codes for
    backward compatibility (JEP-0086).
  * legacy authentication ("digest" and "plain") (JEP-0078).
  * component protocol (JEP-0114).
  * Service Discovery (JEP-0030).
  * vCards -- both Jabber "vcard-temp" and RFC 2426
  * basic parts of the Multi-User Chat protocol (JEP-0045)
  * delayed delivery timestamps (JEP-0091).
 .
 This package contains the API documentation of PyXMPP.
